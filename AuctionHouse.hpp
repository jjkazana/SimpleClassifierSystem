#ifndef AUCTION_HOUSE_GUARD
#define AUCTION_HOUSE_GUARD

#include"GA/population.hpp"
#include"message.hpp"
#include"classifier.hpp"
#include"response.hpp"

template<typename G, typename A>
class AuctionHouse{

	public:
	AuctionHouse(int indv_ammount, int genome_length, int effector_amount);
	AuctionHouse(std::string classifier_filename, std::string effector_filename);	


	void 									load_messages_from(auto& container);
	void 									bucket_brigade();
	void									replace_classifiers();
	msg::message<std::vector<G>>	winner_for(auto& message);

	void 									run();

	class Solution{
		std::vector<G> found;
	};

	population<G,A> 									  pop;
	population<G,A> 									  eff;
	std::vector<classifier<G,A>>					  classifier_list;
	std::vector< msg::message<std::vector<G>> > message_list;
	std::vector< msg::message<std::vector<G>> > tmp_msg_list;
};

template<typename G, typename A>
AuctionHouse<G,A>::AuctionHouse( int classifier_ammount, int classifier_length, 
											int effector_amount) :
	pop(classifier_ammount, classifier_length*2, 1),
	eff(effector_amount, classifier_length*2, 1)
{
	for(auto x : pop.indv)
		classifier_list.push_back( classifier<G,A>(x, Std) );

	for(auto x : eff.indv)
		classifier_list.push_back( classifier<G,A>(x, Effector) );
}


template<typename G, typename A>
AuctionHouse<G,A>::AuctionHouse( std::string classifiers_file, 
											std::string effector_file) :
	pop(classifiers_file, 1),
	eff(effector_file, 1)
{
	for(auto x : pop.indv)
		classifier_list.push_back( classifier<G,A>(x, Std) );

	for(auto x : eff.indv)
		classifier_list.push_back( classifier<G,A>(x, Effector) );
}


template<typename G, typename A>
void 
AuctionHouse<G,A>::load_messages_from(auto& m_container){

	for(auto x:m_container)
		message_list.push_back( msg::message<std::vector<G>>(msg::Initial, x) );
}


template<typename G, typename A>
void 
AuctionHouse<G,A>::bucket_brigade(){

	for(auto message:message_list)
		tmp_msg_list.push_back(winner_for(message.content));

	message_list = tmp_msg_list;
	tmp_msg_list.clear();
}


template<typename G, typename A>
void 
AuctionHouse<G,A>::replace_classifiers(){

	classifier_list.clear();

	for(auto x:pop.indv)
		classifier_list.push_back(classifier<G,A>(x,Std));

	for(auto x:eff.indv)
		classifier_list.push_back(classifier<G,A>(x,Effector));
}


template<typename G, typename A>
msg::message<std::vector<G>>
AuctionHouse<G,A>::winner_for(auto& message){

	msg::message<std::vector<G>> ret_msg;
	

	auto classifier = classifier_list.begin();
	int for_cls_nr=0;
	A highest_bid = 0;

	for(; classifier != classifier_list.end(); for_cls_nr++, classifier++)
		if(*classifier.condition_match_to(message))
			if(*classifier.bid_for(message) > highest_bid)
				ret_msg = *classifier.message(for_cls_nr);
	
	
	auto* winner = &classifier_list[ret_msg.author];
 	
	if( winner->type = Effector) throw Solution{winner.message};
	
	winner->adaptation -= highest_bid;
	classifier_list[message.author].adaptation += highest_bid;

	return ret_msg;
}



template<typename G, typename A>
void 
AuctionHouse<G,A>::run(){

	for(int i=0; i<2000; i++){
		
		if(i%20 == 0){
			pop.evolve();
			eff.evolve();
			replace_classifiers();
		}

		try{
			bucket_brigade();			
		}
		catch(Solution solution){
			sygnalize(solution.found);
		}
	}
}

#endif
