#ifndef RESPONSE_GUARD
#define RESPONSE_GUARD

void
sygnalize(auto solution){

	for(auto x:solution)
		std::cout << x;

	std::cout << "\n"; 
}

#endif
