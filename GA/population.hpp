#ifndef INDIVIDUAL_GUARD
#define INDIVIDUAL_GUARD

#include<vector>
#include<iostream>
#include<cstdlib>
#include<string>
#include<sstream>
#include<fstream>

#include<boost/random/random_device.hpp>

//	G - genome data type
//	A - adaptation data type

template<typename G, typename A>
class individual{

	public:
	individual<G,A>(int length);
	individual<G,A>(individual<G,A>& parent1, individual<G,A>& parent2);
	individual<G,A>(std::string);
	
//	void				   calc_adaptation();
	void 				   invert_rand_part();
	individual<G,A>   crossed_with(individual<G,A>& second_parent);
	individual<G,A>&  choosen_partner_from(auto& population_container);
	A 						preferences_for(individual<G,A>& );
	void 					load(auto& genotype, A& adapt);

	friend std::ostream& operator<<(std::ostream& os, individual& id){
		for(auto x:id.genome)
			os << x << ',';

		os << ',' << id.adaptation;
		return os;
	}


	std::vector<G> genome;
	A adaptation;
	int age;
//	float inv_probability = 0.5;	//inversion probability
};



#include"user_written.hpp"
template<typename G, typename A>
individual<G,A>::individual(int length) : genome(length), age(0){
	
					 user::fill_with_vals(genome);		// !genome is modified!
	adaptation = user::calc_adaptation<A>(genome);
}



template<typename G, typename A>
individual<G,A>::individual(individual<G,A>& par1, individual<G,A>& par2) :
	 genome(par1.genome.size()), age(0){
	
	boost::random::random_device rnd;	
	
	auto slice = rnd();	//find slicing point in genome
	slice %= par1.genome.size()+1;
	
	this->genome = par1.genome;							//fill genome with first parent part
	
	for(auto i=slice; i<this->genome.size(); i++)		//fill with second parent part
		genome[i] = par2.genome[i];		

	adaptation = user::calc_adaptation<A>(genome);
}




template<typename G, typename A>
individual<G,A>::individual(std::string str): age(0){

	//start at the end
	auto adapt_end = str.end();
	auto adapt_begin = str.end();
	
	//last element is an adaptation
	while(*adapt_begin != ' ')
		adapt_begin--;

	std::string adapt_substr(adapt_begin,adapt_end);
	std::stringstream adapt(adapt_substr);

	adapt >> adaptation; 
	
	//and rest are gens
	str.erase(adapt_begin, str.end());
	std::stringstream gen(str);
	G g;

	while(gen >> g)
		genome.push_back(g);
}
/*
template<typename G, typename A>
void individual<G,A>::invert_rand_part(){

}
*/


template<typename G, typename A>
individual<G,A> 
individual<G,A>::crossed_with(individual<G,A>& second_parent){

	return individual<G,A>(*this, second_parent);
}



template<typename G, typename A>
A 
individual<G,A>::preferences_for(individual<G,A>& ind2){

	A ret(0);

	for(int i=0; i<this->genome.size(); i++)
		genome[i] == ind2.genome[i] ? ret-- : ret++ ;

	return ret/100;
}


template<typename G, typename And>
void 
individual<G,And>::load(auto& new_genotype, And& adapt){
	
	for(int i=0; i<genome.size(); i++)
		genome[i] = new_genotype[i];

	adaptation = adapt;
}


template<typename G, typename A>
individual<G,A>& 
individual<G,A>::choosen_partner_from(auto& population){
	
	std::vector<A> sub_adaptation; //sub = subjective

	for(auto x:population)
		sub_adaptation.push_back( x.adaptation + this->preferences_for(x) );

	return population[ user::choosen_individual_by(sub_adaptation) ];
}





//population is considered as set of individuals with 
//genom of type G, and adaptation values of type A

template<typename G, typename A>
class population{

	public:
	population(int indv_ammount, int genome_length, int generations); 
	population(std::string indv, int generations);
	

	friend std::ostream& operator<<(std::ostream& os, population& pop){
		for(auto x:pop.indv)
			os << x << "\n";

		return os;
	}


	individual<G,A>& first_parent();	
	void evolve();
	A average_adaptation();

	std::vector<individual<G,A>> indv;	//population container, indv - individual(s)
	std::vector<individual<G,A>> tmp;	//here lands newly created individuals

	int generations_to_run;	
};

//****************************

template<typename G, typename A>
population<G,A>::population(int indv_amm, int g_length, int gen): 
			generations_to_run(gen)
{
	for(int i=0; i<indv_amm; i++)
		indv.push_back(individual<G,A>(g_length));
}


template<typename G, typename A>
population<G,A>::population(std::string filename, int gen):
		generations_to_run(gen)
{

	std::ifstream from_file(filename);
	std::string current_line;

	while(std::getline(from_file, current_line) && !from_file.eof())
		indv.push_back(individual<G,A>(current_line));

} 


template<typename A>
std::vector<A> 
adaptations_from(auto population){
	std::vector<A> ret;

	for(auto indv:population)
		ret.push_back(indv.adaptation);
	
	return ret;
}


template<typename G, typename a_set_of>
individual<G,a_set_of>& 
population<G,a_set_of>::first_parent(){

	return indv[ user::choosen_individual_by( adaptations_from<a_set_of>(indv) ) ];
}



template<typename G, typename A>
void 
population<G,A>::evolve(){

	for(int i=0; i<generations_to_run; i++){
		while(tmp.size()<indv.size()){
		
			individual<G,A> f_parent = first_parent();
			individual<G,A>* his = &f_parent;
 			//line below is actual behaviour of this loop
			tmp.push_back( f_parent.crossed_with( his->choosen_partner_from(indv) ) );
			}

		for(auto x: user::swap_indv_from( indv,/*with ones from*/ tmp ))
			indv[x.first] = tmp[x.second];

		tmp.clear();
	}

	for(auto x = indv.begin(); x != indv.end(); x++)
		(*x).age++;
}

template<typename G, typename A>
A 
population<G,A>::average_adaptation(){

	A ret(0);

	for(auto x:indv)
		ret += x.adaptation;	

	return ret/indv.size();
}

#endif
