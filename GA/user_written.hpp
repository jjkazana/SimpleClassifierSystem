#ifndef USER_WRITTEN_GUARD
#define USER_WRITTEN_GUARD

#include<string>
#include<utility>
#include<algorithm>

#include<boost/random/random_device.hpp>
#include<boost/random/uniform_int_distribution.hpp>

#include<cmath>
namespace user{
	
	//A - adaptation data typename
	//C - genom container type
	template<typename A, typename C>
	A calc_adaptation(C genome){
		A ret(10);	
	
		return ret;
	}

	//this function is used to initially fill genome, by constructor individual<G,A>
	//(int length)
	//C - genom container type
	template<typename C>
	void fill_with_vals(C& genome){
		
		std::string gens{"01#"};
		std::string message{"01"};	
		boost::random::uniform_int_distribution<> dist_c(0,2);
		boost::random::uniform_int_distribution<> dist_m(0,1);
		boost::random::random_device gen;

		auto x = genome.begin();
		for(; x != genome.begin() + genome.size()/2; x++)
			*x = gens[dist_c(gen)];

		for(; x!=genome.end(); x++)
			*x = message[dist_m(gen)];
	}


	//returns number of choosen individual (in population) for selecting first and second parent.
	template<typename C>
	int choosen_individual_by(C adaptation_of/*individual*/){
	
		//use tournament to find parent
		std::vector<int> competitors;
		const int COMP_AMMOUNT = 10;

		boost::random::uniform_int_distribution<> dist(0,adaptation_of.size()-1);
		boost::random::random_device gen;

		//find rand subpopulation
		for(int i=0; i<COMP_AMMOUNT ; i++)
			competitors.push_back( dist(gen) );

		int best_competitor( competitors[0] );
		for(auto competitor : competitors)
			if( adaptation_of[competitor] > adaptation_of[best_competitor] )
				best_competitor = competitor;

		return best_competitor;
	}


	template<typename T, typename A>
	std::vector<int> the_worst_of(T pop, int ammount){
		std::vector<int> ret;

		int tmp_index;
		A tmp_adapt(1000000);

		for(int i=0; i<ammount; i++){
			//find 10 of the worst indv in old generation
			for(int indv=0; indv<pop.size(); indv++)
				if(	tmp_adapt > pop[indv].adaptation
					&& std::find(ret.begin(), ret.end(), pop[indv].adaptation ) 
					== ret.end() ){

						tmp_adapt = pop[indv].adaptation;
						tmp_index = indv;						
						}

			ret.push_back(tmp_index);
		}
	return ret;
	}

	template<typename T, typename A>
	std::vector<int> the_best_of(T pop, int ammount){
		std::vector<int> ret;

		int tmp_index;
		A tmp_adapt(-10000000);

		for(int i=0; i<ammount; i++){
			//find 10 of the worst indv in old generation
			for(int indv=0; indv<pop.size(); indv++)
				if(	tmp_adapt < pop[indv].adaptation
					&& std::find(ret.begin(), ret.end(), pop[indv].adaptation ) 
					== ret.end() ){

						tmp_adapt = pop[indv].adaptation;
						tmp_index = indv;						
						}

			ret.push_back(tmp_index);
		}
	return ret;
	}

	//any object/struct representing pairs with arguments first and second is accepted
	//returns pairs to be swapped	 
	template<typename generation>
	std::vector<std::pair<int,int>> swap_indv_from(generation old,/*with*/generation n3w){

		std::vector<std::pair<int,int>> ret;

		std::vector<int> to_be_replaced;
		std::vector<int> replace_with;
		
		int tmp_index(0);

		int indv_amm = 12;

		to_be_replaced = the_worst_of<generation, double>(old, indv_amm);
		replace_with = the_best_of<generation, double>(n3w, indv_amm);

		for(int i=0; i<to_be_replaced.size() && i<replace_with.size(); i++)
			ret.push_back( std::pair<int,int>( to_be_replaced[i], replace_with[i] ) );

		return ret;
	}
	

};



#endif
