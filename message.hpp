#ifndef MESSAGE_GUARD
#define MESSAGE_GUARD

namespace msg{

	enum m_type{
		Initial, Std
	};
	
	template<typename C>
	struct message{
		
		m_type type;
		C 		 content;
		int 	 author;
	
		message(C cont, int auth ,m_type t) : content(cont), type(t), author(auth)
		{
		}
	};
}
#endif
