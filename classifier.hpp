#ifndef CLASSIFIER_GUARD
#define CLASSIFIER_GUARD

#include<iostream>

#include"GA/population.hpp"
#include<algorithm>
#include"message.hpp"


enum c_type{
	Std, Effector
};

//whereever "auto message" is used message is understand as message.content, as there was no message struct
template<typename G, typename A>
class classifier{

	public:
	classifier(individual<G,A>, c_type);
	
	A 									  bid_for(auto& message);
	msg::message<std::vector<G>> message(int nr);
	bool 								  condition_match_to(auto& message);

	friend std::ostream& operator<<(std::ostream& os, classifier<G,A>& c){
		for(auto x:c.condition)
			os << x;

		os<<" : ";
		
		for(auto x:c.reaction)
			os << x;

		return os;
	}

	std::vector<G> condition;
	std::vector<G>	reaction;
	A 					strength;
	c_type 			type;
	A 					constant = 0.1;
};

template<typename G, typename A>
classifier<G,A>::classifier(individual<G,A> ind, c_type t):
		condition(ind.genome.size()/2),
		reaction(ind.genome.size()/2), 
		strength(ind.adaptation), 
		type(t)
{
	std::copy( ind.genome.begin(), ind.genome.begin()+ind.genome.size()/2, 						condition.begin() );
	std::copy( ind.genome.begin()+ind.genome.size()/2, ind.genome.end(),
					reaction.begin() );
}


template<typename G, typename A>
A 
classifier<G,A>::bid_for(auto& message){

	A ret(0);

	for(int i=0; i<condition.size(); i++)
		if(message[i] == condition[i]) ret++;

	return  ret*strength*constant;
}


template<typename G, typename A>
msg::message<std::vector<G>> 
classifier<G,A>::message(int of_cls_nr){

	return msg::message<std::vector<G>>(reaction, of_cls_nr, msg::Std);
}


template<typename G, typename A>
bool
classifier<G,A>::condition_match_to(auto& message){

	auto msg = message.begin();
	auto msg_end = message.end();

	auto cond = this->condition.begin();
	auto cond_end = this->condition.end();

	for( ; msg != msg_end && cond != cond_end ; msg++, cond++)
		if(*msg != (G)'#' && *msg != *cond)
			return false;
			  
	return true;
}












#endif
